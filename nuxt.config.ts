// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  css: [
    'vuetify/lib/styles/main.sass',
    '@/assets/css/tailwind.scss',
    'assets/css/style.scss',
  ],
  build: {
    transpile: ['vuetify'],
  },
  modules: ['@nuxtjs/tailwindcss', '@vueuse/nuxt', '@pinia/nuxt']

})
